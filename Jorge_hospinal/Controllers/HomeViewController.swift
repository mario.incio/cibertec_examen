//
//  HomeViewController.swift
//  Jorge_hospinal
//
//  Created by Jorge hospinal on 4/06/21.
//

import UIKit
import FirebaseFirestore
class HomeViewController: UIViewController {

    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var namesTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    private let email: String
    private let password: String
    private let db = Firestore.firestore()
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
        super.init(nibName: "HomeViewController", bundle: Bundle.main)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Actualizar Datos"
        self.emailTextField.text = self.email
        self.passTextField.text = self.password
        
        db.collection("users").document(email).getDocument(completion: {
            (documentSnapshot, error) in
            if let document = documentSnapshot, error == nil {
                if let name = document.get("name") as? String, let id = document.get("uid") as? String {
                    self.namesTextField.text = name
                    self.idTextField.text = id
                    
                }
            }
        })
        
        
    }
    
    @IBAction func updateUsersButton(_ sender: Any) {
        if let email = emailTextField.text, let pass = passTextField.text {
            self.db.collection("users").document(email).setData([
                "name": self.namesTextField.text,
                "password": self.passTextField.text,
            ])
            
            let alertController = UIAlertController(title: "CORRECTO", message: "Usuario actualizado correctamente", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteUsersButton(_ sender: Any) {
        view.endEditing(true)
        db.collection("users").document(email).delete()
        let alertController = UIAlertController(title: "CORRECTO", message: "Usuario eliminado correctamente", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
