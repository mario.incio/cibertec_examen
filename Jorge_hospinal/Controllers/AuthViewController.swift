//
//  ViewController.swift
//  Jorge_hospinal
//
//  Created by Jorge Hospinal on 3/06/21.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class AuthViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBAction func registerButton(_ sender: Any) {
        self.navigationController?.pushViewController(RegisterViewController(), animated: true)
    }
    
    private let db = Firestore.firestore()
    
    @IBAction func clickLoginButton(_ sender: Any) {
        if let email = emailTextField.text, let pass = passwordTextField.text {
        Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
            if let result = result, error == nil {
                
                
                
                self.navigationController?.pushViewController(HomeViewController(email: email, password: pass), animated: true)
                let alertController = UIAlertController(title: "CORRECTO", message: "Usuario Logueado correctamente", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Usuario y/o contraseña inválida", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
        }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Autenticacción"
        // Do any additional setup after loading the view.
    }


}

