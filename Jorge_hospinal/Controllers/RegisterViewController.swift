//
//  RegisterViewController.swift
//  Jorge_hospinal
//
//  Created by Jorge Hospinal on 3/06/21.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class RegisterViewController: UIViewController {

    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var repassTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    
    private let db = Firestore.firestore()
    
    
    @IBAction func registerTextField(_ sender: Any) {
        view.endEditing(true)
        
        if let email = emailTextField.text, let pass = passTextField.text {
        Auth.auth().createUser(withEmail: email, password: pass) { (result, error) in
            if let result = result, error == nil {
                self.db.collection("users").document(email).setData([
                    "uid": result.user.uid,
                    "name": self.nameTextField.text,
                    "email": self.emailTextField.text,
                    "password": self.passTextField.text,
                    "typeUser": self.typeTextField.text
                ])
                let alertController = UIAlertController(title: "CORRECTO", message: "Usuario registrado correctamente", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
            else {
                let alertController = UIAlertController(title: "Error", message: "Se produjo un error", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
                self.present(alertController, animated: true, completion: nil)
            }
        }
        }
        
    }
    
    
    func mensaje(message: String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Aceptar", style: .default))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Resgistrar usuaarios"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
